﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ComandaApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new ComandaApp.MainPage();
            //MainPage = new NavigationPage(new ComandaApp.Pages.HomePage());
            //MainPage = new ComandaApp.Pages.ListaCadastroProdutosPage();
            MainPage = new ComandaApp.Pages.ListaComandasPage();
            //MainPage = new ComandaApp.Pages.ListaPedidosComandaPage();
            //MainPage = new ComandaApp.Pages.LoginPage();
            //MainPage = new ComandaApp.Pages.RedefinirSenhaPage();
            //MainPage = new ComandaApp.Pages.CadastroPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
